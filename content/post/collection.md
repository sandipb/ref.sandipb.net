---
title: "Welcome"
date: 2017-12-07T17:33:24-08:00
draft: false
---

This is just a collection of my references on various topics.

Current docs include:

- <span class="fa fa-book" aria-hidden="true"></span> [Distributed Systems](/distributed_systems)
- <span class="fa fa-book" aria-hidden="true"></span> [Golang](/golang)
- <span class="fa fa-book" aria-hidden="true"></span> [Docker](/docker)
- <span class="fa fa-book" aria-hidden="true"></span> [Git](/git)