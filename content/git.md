---
title: "Git"
date: 2017-12-07T19:15:21-08:00
draft: false
---

# References

- [Using Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules): No matter how less I want to use Git modules, I seem to need them all the time. Most of the time, whenever I use a program like [Hugo] which has external extensions, I end up needing submodules.

# Common procedures

- [Remove a Submodule within git](https://davidwalsh.name/git-remove-submodule): Adding a submodule is easy, removing is such a pain

[hugo]: https://gohugo.io/