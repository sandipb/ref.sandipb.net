---
title: Docker
date: 2017-12-05T20:03:00.000+00:00

---
# Basics

- [Cgroups, namespaces, and beyond: what are containers made from?](https://www.youtube.com/watch?v=sK5i-N34im8): <span class="fa fa-youtube fa-lg" aria-hidden="true"></span> <em>Dec 3, 2015, Jérôme Petazzoni</em>

# Specs

- [Docker Image Specification v1.0.0](https://github.com/moby/moby/blob/master/image/spec/v1.md) <span class="fa fa-github fa-lg" aria-hidden="true"></span> 


# Swarm

- [Docker 1.12 Swarm Mode Deep Dive Part 1: Topology](https://www.youtube.com/watch?v=dooPhkXT9yI): <span class="fa fa-youtube fa-lg" aria-hidden="true"></span> <em>Jul 28, 2016</em>
- [Docker 1.12 Swarm Mode Deep Dive Part 2: Orchestration](https://www.youtube.com/watch?v=_F6PSP-qhdA): <span class="fa fa-youtube fa-lg" aria-hidden="true"></span> <em>Jul 28, 2016</em>

# Books

- [The Docker book](https://dockerbook.com/)
- [Docker Deep Dive](https://www.amazon.com/Docker-Deep-Dive-Nigel-Poulton/dp/1521822808/)